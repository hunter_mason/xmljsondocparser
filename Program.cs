﻿using XmlJsonDocParser.Models;
using static XmlJsonDocParser.Utilities.Parser;

namespace XmlJsonDocParser
{
    /// <summary>
    /// The class that runs the main functionality of the XmlJsonParser.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Name of the document to parse and export.
        /// </summary>
        public static string FILE_NAME = "test";

        /// <summary>
        /// The xml document to parse.
        /// </summary>
        public static string XML_DOCUMENT = Path.Combine(Directory.GetCurrentDirectory(), "..", "..", "..", "Resources", $"{FILE_NAME}.xml");

        /// <summary>
        /// The json document to export.
        /// </summary>
        public static string JSON_DOCUMENT = Path.Combine(Directory.GetCurrentDirectory(), "..", "..", "..", "Resources", $"{FILE_NAME}.json");
        
        /// <summary>
        /// Parses xml properties to json.
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            List<Property> properties = ImportXml<Property>(XML_DOCUMENT, "member", "members");
            Dictionary<string, string> document = PopulateDocument(ref properties);
            ExportJson(JSON_DOCUMENT, ref document);
        }
    }
}