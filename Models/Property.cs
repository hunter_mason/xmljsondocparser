﻿using Newtonsoft.Json;
using System.Xml.Serialization;

namespace XmlJsonDocParser.Models
{
    /// <summary>
    /// A property container to store in the document.
    /// </summary>
    [Serializable]
    [XmlRoot("member")]
    public class Property
    {
        /// <summary>
        /// The property key.
        /// </summary>
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// The property value.
        /// </summary>
        [XmlElement(ElementName = "summary")]
        public string Summary { get; set; }

        /// <summary>
        /// Flag to keep track of the valid status.
        /// </summary>
        [JsonIgnore]
        public bool IsValid = true;

        /// <summary>
        /// The token to compare against the first character of the "name" attribute.
        /// </summary>
        private const char TOKEN = 'P';

        /// <summary>
        /// Validates the property.
        /// Is invalid if it's not a property.
        /// </summary>
        public void Validate()
        {
            if (Name != null)
            {
                if (Name.First() != TOKEN)
                {
                    IsValid = false;
                    return;
                }

                Name = Name.TrimStart(TOKEN, ':');

                string[] segments = Name.Split('.');
                if (segments.Length > 2)
                {
                    Name = string.Join('.', segments.Skip(segments.Length - 2));
                }
            }

            if (Summary != null)
            {
                Summary = Summary.Trim();
            }
        }
    }
}
