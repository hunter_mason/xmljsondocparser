﻿using Newtonsoft.Json;
using System.Xml;
using System.Xml.Serialization;
using XmlJsonDocParser.Models;
using Formatting = Newtonsoft.Json.Formatting;

namespace XmlJsonDocParser.Utilities
{
    /// <summary>
    /// A class containing xml and json parsing utilities.
    /// </summary>
    public class Parser
    {
        /// <summary>
        /// Settings for the XmlReader.
        /// </summary>
        public static XmlReaderSettings XmlSettings = new() { ConformanceLevel = ConformanceLevel.Fragment };

        /// <summary>
        /// Settings for the json serializer.
        /// </summary>
        public static JsonSerializerSettings JsonSettings = new() { Formatting = Formatting.Indented };

        /// <summary>
        /// Loads elements of an xml file into a list.
        /// </summary>
        /// <typeparam name="T">The object type to serialize.</typeparam>
        /// <param name="xmlPath">The path to the xml file being imported.</param>
        /// <param name="element">The element in the xml to load.</param>
        /// <param name="stopToken">The token to fast forward to.</param>
        /// <returns>A list of objects matching the element.</returns>
        public static List<T> ImportXml<T>(string xmlPath, string element, string stopToken)
        {
            using StreamReader stream = new(File.Open(xmlPath, FileMode.Open));
            using XmlReader reader = XmlReader.Create(stream, XmlSettings);

            XmlSerializer serializer = new(typeof(T));

            List<T> objs = new();

            // Fast-forward
            while (reader.Read())
            {
                if (reader.Name.Equals(stopToken)) break;
            }

            // Read the objects.
            while (reader.Read())
            {
                if (!reader.Name.Equals(element)) continue;

                objs.Add((T)serializer.Deserialize(reader));
            }

            return objs;
        }

        /// <summary>
        /// Populates a dictionary with the properties.
        /// </summary>
        /// <param name="properties">The properties list being validated and pushed into the dictionary.</param>
        /// <returns>A dictionary of validated properties.</returns>
        public static Dictionary<string, string> PopulateDocument(ref List<Property> properties)
        {
            Dictionary<string, string> document = new();
            foreach (Property property in properties)
            {
                property.Validate();
                if (property.IsValid)
                {
                    document.Add(property.Name, property.Summary);
                }
            }

            return document;
        }

        /// <summary>
        /// Exports a dictionary as a json file.
        /// </summary>
        /// <param name="jsonPath">The destination of the exported json file.</param>
        /// <param name="document">The dictionary to export.</param>
        public static void ExportJson(string jsonPath, ref Dictionary<string, string> document)
        {
            if (File.Exists(jsonPath))
            {
                File.Delete(jsonPath);
            }

            using FileStream stream = File.Open(jsonPath, FileMode.OpenOrCreate);
            using StreamWriter writer = new(stream);

            JsonSerializer serializer = JsonSerializer.Create(JsonSettings);
            serializer.Serialize(writer, document);
        }
    }
}
